﻿using OntologyClasses.DataClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using WordPress_Module;
using WordPress_Module.Factory;
using WordpressLearn.BaseClasses;
using WordPressSharp;

namespace WordpressLearn
{
    public class MainWindowViewModel : ViewModelBase
    {
        private WordPressOntologyEngine wordPressOntologyEngine;
        private clsLogStates logStates = new clsLogStates();
        
        private string userName;
        public string UserName
        {
            get
            {
                return userName;
            }
            set
            {
                userName = value;
                ValidateActionSource();
                RaisePropertyChanged(NotifyChanges.MainWindow_UserName);
            }
        }

        private string baseUrl;
        public string BaseUrl
        {
            get { return baseUrl; }
            set
            {
                baseUrl = value;
                ValidateActionSource();
                RaisePropertyChanged(NotifyChanges.MainWindow_BaseUrl);
            }
        }

        private Uri BaseUri;

        private string password;
        public string Password
        {
            get { return password; }
            set
            {
                password = value;
                ValidateActionSource();
                RaisePropertyChanged(NotifyChanges.MainWindow_Password);
            }
        }

        private BlogQueryInfoValidationResult lastQueryInfoValidationResult;
        public BlogQueryInfoValidationResult LastQueryInfoValidationResult
        {
            get { return lastQueryInfoValidationResult; }
            set
            {
                lastQueryInfoValidationResult = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_LastQueryInfoValidationResult);
            }
        }

        private PostListInfoValidationResult lastPostListInfoValidationResult;
        public PostListInfoValidationResult LastPostListInfoValidationResult
        {
            get { return lastPostListInfoValidationResult; }
            set
            {
                lastPostListInfoValidationResult = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_LastPostListInfoValidationResult);
            }
        }

        private List<UserBlog> userBlogs = null;
        public List<UserBlog> UserBlogs
        {
            get { return userBlogs; }
            set
            {
                userBlogs = value;
                ValidateActionSource();
                RaisePropertyChanged(NotifyChanges.MainWindow_UserBlogs);
            }
        }

        private UserBlog selectedUserBlog;
        public UserBlog SelectedUserBlog
        {
            get
            {
                return selectedUserBlog;
            }
            set
            {
                selectedUserBlog = value;
                ValidateActionSource();
                RaisePropertyChanged(NotifyChanges.MainWindow_SelectedUserBlog);
            }
        }

        private void ValidateActionSource()
        {
            LastQueryInfoValidationResult = ValidationEngine.IsOk_BlogQueryInfo(userName, password, baseUrl);
            IsEnabled_GetBlogs = LastQueryInfoValidationResult == BlogQueryInfoValidationResult.None;
            QueryInfoErrorColor = LastQueryInfoValidationResult == BlogQueryInfoValidationResult.None ? Brushes.Transparent : Brushes.Red;
            LastPostListInfoValidationResult = ValidationEngine.IsOk_PostListInfo(UserName, password, baseUrl, selectedUserBlog);
            IsEnabled_PostList = LastPostListInfoValidationResult == PostListInfoValidationResult.None;
        }

        private bool isEnabled_GetBlogs;
        public bool IsEnabled_GetBlogs
        {
            get
            {
                return isEnabled_GetBlogs;
            }
            set
            {
                isEnabled_GetBlogs = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_IsEnabled_GetBlogs);
            }
        }

        private bool isEnabled_PostsList;
        public bool IsEnabled_PostList
        {
            get
            {
                return isEnabled_PostsList;
            }
            set
            {
                isEnabled_PostsList = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_IsEnabled_PostList);

            }
        }

        private Brush queryInfoErrorColor;
        public Brush QueryInfoErrorColor
        {
            get { return queryInfoErrorColor; }
            set
            {
                queryInfoErrorColor = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_QueryInfoErrorColor);
            }
        }

        private ObservableCollection<GridPostItem> postList;
        public ObservableCollection<GridPostItem> PostList
        {
            get { return postList; }
            set
            {
                postList = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_PostList);
            }

        }

        public void GetPostList()
        {
            
            if (SelectedUserBlog != null)
            {
                var result = wordPressOntologyEngine.GetPostList(SelectedUserBlog, BaseUrl, UserName, Password, true);

                
                if (result.GUID == logStates.LogState_Success.GUID)
                {
                    PostList = new ObservableCollection<GridPostItem>(wordPressOntologyEngine.PostList.Select(postItem => new GridPostItem(postItem)));
                }
                
                
            }
            
        }

        public MainWindowViewModel()
        {
            wordPressOntologyEngine = new WordPressOntologyEngine();
        }

        public void GetUserBlogs()
        {
            var result = wordPressOntologyEngine.GetUserBlogs(BaseUrl, UserName, Password, true);
            if (result.GUID == logStates.LogState_Success.GUID)
            {
                UserBlogs = wordPressOntologyEngine.UserBlogs;
            }
        }
    }

}
