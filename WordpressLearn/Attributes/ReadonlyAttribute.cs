﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordpressLearn.Attributes
{
    public class ReadonlyAttribute : Attribute
    {
        public bool IsReadonly { get; set; }

        public ReadonlyAttribute(bool isReadonly)
        {
            IsReadonly = isReadonly;
        }
    }
}
