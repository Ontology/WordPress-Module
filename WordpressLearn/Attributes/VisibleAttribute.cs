﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordpressLearn.Attributes
{
    public class VisibleAttribute : Attribute
    {
        public bool IsVisible { get; set; }


        public VisibleAttribute(bool isVisible)
        {
            this.IsVisible = isVisible;
        }
    }

}
