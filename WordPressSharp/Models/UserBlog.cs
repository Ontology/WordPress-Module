﻿using CookComputing.XmlRpc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordPressSharp.Models
{
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public class UserBlog
    {
        [XmlRpcMember("blogid")]
        public string BlogId { get; set; }

        [XmlRpcMember("blogName")]
        public string BlogName { get; set; }

        [XmlRpcMember("url")]
        public string Url { get; set; }

        [XmlRpcMember("xmlrpc")]
        public string XmlRpc { get; set; }

        [XmlRpcMember("isAdmin")]
        public bool IsAdmin { get; set; }
    }
}
