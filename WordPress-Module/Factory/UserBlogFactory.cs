﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordPress_Module.Factory
{
    public static class UserBlogFactory
    {
        public static List<UserBlog> CreateUserBlogs(WordPressSharp.Models.UserBlog[] userBlogs)
        {
            return userBlogs != null ?  userBlogs.Select(userBlog => new UserBlog(userBlog)).ToList() : null;
        }
    }
}
