﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordPress_Module.Factory
{
    public static class TermFactory
    {
        public static List<TermItem> CreateTermList(WordPressSharp.Models.Term[] terms)
        {
            return terms != null ?  terms.Select(termItem => new TermItem(termItem)).ToList() : null;
            
        }
    }
}
