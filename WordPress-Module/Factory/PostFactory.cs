﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordPress_Module.Factory
{
    public static class PostFactory
    {
        public static List<PostItem> CreatePostList(WordPressSharp.Models.Post[] postList, UserBlog parentBlog)
        {
            return postList != null ? postList.Select(postItem => new PostItem(postItem) { UserBlog = parentBlog }).ToList() : new List<PostItem>();
        }
    }
}
