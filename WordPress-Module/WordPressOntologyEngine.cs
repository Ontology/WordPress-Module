﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordPress_Module.BaseClasses;
using WordPress_Module.Factory;
using WordPressSharp;

namespace WordPress_Module
{
    public class WordPressOntologyEngine : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;
        private OntologyModDBConnector dbWriter;
        private List<UserBlog> userBlogs;
        public List<UserBlog> UserBlogs
        {
            get { return userBlogs; }
            set
            {
                userBlogs = value;
                RaisePropertyChanged(NotifyChanges.WordPressOntologyEngine_UserBlogs);
            }
        }

        private List<PostItem> postList;
        public List<PostItem> PostList
        {
            get { return postList; }
            set
            {
                postList = value;
                RaisePropertyChanged(NotifyChanges.WordPressOntologyEngine_PostList);
            }
        }

        public clsOntologyItem GetUserBlogs(string baseUrl, string userName, string password, bool sync)
        {
            try
            {
                using (var client = new WordPressClient(new WordPressSiteConfig
                {
                    BaseUrl = baseUrl,
                    Username = userName,
                    Password = password
                }))
                {
                    var userBlogs = client.GetUserBlogs();
                    UserBlogs = UserBlogFactory.CreateUserBlogs(userBlogs);

                    if (sync)
                    {
                        return dbWriter.SaveModel(UserBlogs.Select(blog => blog).ToList<object>());
                    }
                }
                return localConfig.Globals.LState_Success.Clone();
            }
            catch (Exception ex)
            {
                return localConfig.Globals.LState_Error.Clone();
            }
            
        }

        public clsOntologyItem GetPostList(UserBlog userBlog, string baseUrl, string userName, string password, bool sync)
        {
            var result = localConfig.Globals.LState_Error.Clone();
            if (userBlog != null && !string.IsNullOrEmpty(baseUrl) && !string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password))
            {
                try
                {
                    using (var client = new WordPressClient(new WordPressSiteConfig
                    {
                        BaseUrl = baseUrl,
                        Username = userName,
                        Password = password,
                        BlogId = userBlog.BlogId
                    }))
                    {
                        var postList = new List<PostItem>();
                        var offset = 0;
                        var number = 100;
                        var count = -1;
                        while (count != 0)
                        {
                            var addList = PostFactory.CreatePostList(client.GetPosts(new WordPressSharp.Models.PostFilter { Offset = offset, Number = number }), userBlog);
                            count = addList.Count;
                            postList.AddRange(addList);
                            offset += number;
                        }


                        PostList = postList;
                        var terms = new List<TermItem>();
                        PostList.ForEach(post =>
                        {
                            terms.AddRange(post.TermItems);
                        });
                        var modelItems = PostList.Select(blog => blog).ToList<object>();
                        modelItems.AddRange(terms.Select(term => term).ToList<object>());
                        result = dbWriter.SaveModel(modelItems);
                    }

                }
                catch(Exception ex)
                {
                    
                }
                
            }
            

            return result;
        }
        public WordPressOntologyEngine(Globals globals)
        {
            localConfig = new clsLocalConfig(new Globals());
            Initialize();
        }
        public WordPressOntologyEngine()
        {
            localConfig = new clsLocalConfig(new Globals());
            Initialize();
        }

        private void Initialize()
        {
            dbWriter = new OntologyModDBConnector(localConfig.Globals);
        }
    }
}
