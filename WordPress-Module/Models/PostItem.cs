﻿using OntologyAppDBConnector.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using WordPress_Module.BaseClasses;
using WordPress_Module.Factory;

namespace WordPress_Module
{
    [OntologyClass(IdClass = "d8782656a1e64af089b968ae9c0b81fa")]
    public class PostItem : NotifyPropertyChange
    {
        private string id;
        public string Id
        {
            get { return id; }
            set
            {
                id = value;
                RaisePropertyChanged(NotifyChanges.PostItem_Id);
            }
        }

        [ObjectAttribute(IdAttributeType = "0b183be9c13d4157989b63b0362aeee6", Replace = true)]
        public int IdInt
        {
            get
            {
                int checkInt = 0;
                if (int.TryParse(Id, out checkInt))
                {
                    return checkInt;
                }
                else
                {
                    return 0;
                }
            }
        }

        private string title;
        [OntologyObject(PropertyType = PropertyType.Name)]
        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                RaisePropertyChanged(NotifyChanges.PostItem_Title);
            }
        }

        private DateTime created;
        [ObjectAttribute(IdAttributeType = "b67c3f3cda0346939afcd2014997e328", Replace = true)]
        public DateTime Created
        {
            get { return created; }
            set
            {
                created = value;
                RaisePropertyChanged(NotifyChanges.PostItem_Created);
            }
        }

        private string status;
        public string Status
        {
            get { return status; }
            set
            {
                status = value;
                RaisePropertyChanged(NotifyChanges.PostItem_Status);
            }
        }

        private string type;
        public string Type
        {
            get { return type; }
            set
            {
                type = value;
                RaisePropertyChanged(NotifyChanges.PostItem_Type);
            }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                RaisePropertyChanged(NotifyChanges.PostItem_Name);
            }
        }

        private string author;
        public string Author
        {
            get { return author; }
            set
            {
                author = value;
                RaisePropertyChanged(NotifyChanges.PostItem_Author);
            }
        }

        private string excerpt;
        public string Excerpt
        {
            get { return excerpt; }
            set
            {
                excerpt = value;
                RaisePropertyChanged(NotifyChanges.PostItem_Excerpt);
            }
        }

        private string content;
        [ObjectAttribute(IdAttributeType = "96f4e9490b7e4edaa094d9cb1f49f86c", Replace = true)]
        public string Content
        {
            get { return content; }
            set
            {
                content = value;
                RaisePropertyChanged(NotifyChanges.PostItem_Content);
            }
        }

        private string parent;
        public string Parent
        {
            get { return parent; }
            set
            {
                parent = value;
                RaisePropertyChanged(NotifyChanges.PostItem_Parent);
            }
        }

        private string link;
        [ObjectRelation(IdParentOther = "094d728d6efc463c85c72dcfed903c78", IdRelationType = "92619f7ecbf342308ca34b7e7e8883f6", RelationSearchType = RelationSearchType.Name, Replace = true)]
        public string Link
        {
            get { return link; }
            set
            {
                link = value;
                RaisePropertyChanged(NotifyChanges.PostItem_Link);
            }
        }

        private string guid;
        [OntologyObject(PropertyType = PropertyType.Guid)]
        public string Guid
        {
            get { return guid; }
            set
            {
                guid = value;
                RaisePropertyChanged(NotifyChanges.PostItem_Guid);
            }
        }

        private string commentStatus;
        public string CommentStatus
        {
            get { return commentStatus; }
            set
            {
                commentStatus = value;
                RaisePropertyChanged(NotifyChanges.PostItem_CommentStatus);
            }
        }

        private List<TermItem> termItems;
        [ObjectRelation(IdParentOther = "3bb4b05d8fcb48b7a1ac0fb42a265c38", IdRelationType = "e971160347db44d8a476fe88290639a4", Replace = false, RelationSearchType = RelationSearchType.OntologyItem)]
        public List<TermItem> TermItems
        {
            get { return termItems; }
            set
            {
                termItems = value;
                RaisePropertyChanged(NotifyChanges.PostItem_TermItems);
            }
        }

        private UserBlog userBlog;
        [ObjectRelation(IdParentOther = "3bb4b05d8fcb48b7a1ac0fb42a265c38", IdRelationType = "e07469d9766c443e85266d9c684f944f", Replace = false, RelationSearchType = RelationSearchType.OntologyItem)]
        public UserBlog UserBlog
        {
            get { return userBlog; }
            set
            {
                userBlog = value;

            }
        }

        public PostItem()
        {

        }
        public PostItem(WordPressSharp.Models.Post postItem)
        {
            this.Author = postItem.Author;
            this.CommentStatus = postItem.CommentStatus;
            this.Content = postItem.Content;
            this.Created = postItem.PublishDateTime;
            this.Excerpt = postItem.Exerpt;

            using (MD5 md5 = MD5.Create())
            {
                byte[] hash = md5.ComputeHash(Encoding.Default.GetBytes(postItem.Guid));
                Guid result = new Guid(hash);
                Guid = result.ToString();
            }

            this.Id = postItem.Id;
            this.Link = postItem.Link;
            this.Name = postItem.Name;
            this.Parent = postItem.ParentId;
            this.Status = postItem.Status;
            this.TermItems = TermFactory.CreateTermList(postItem.Terms);
            this.Title = postItem.Title;
            this.Type = postItem.PostType;
        }
    }
}
