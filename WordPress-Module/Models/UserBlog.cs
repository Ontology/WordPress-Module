﻿using OntologyAppDBConnector.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace WordPress_Module
{
    [OntologyClass(IdClass = "00a8483e99a9485ea36b47d4ab1165fe")]
    public class UserBlog
    {
        [ObjectAttribute(IdAttributeType = "0b183be9c13d4157989b63b0362aeee6", Replace = true)]
        public int BlogId { get; set; }
        [OntologyObject(PropertyType = PropertyType.Guid)]
        public string Guid { get; set; }
        [OntologyObject(PropertyType = PropertyType.Name)]
        public string BlogName { get; set; }
        [ObjectRelation(IdParentOther = "094d728d6efc463c85c72dcfed903c78", IdRelationType = "92619f7ecbf342308ca34b7e7e8883f6", RelationSearchType = RelationSearchType.Name, Replace=true)]
        public string Url { get; set; }
        public string XmlRpc { get; set; }
        public bool IsAdmin { get; set; }

        public UserBlog()
        {

        }

        public UserBlog(WordPressSharp.Models.UserBlog userBlog)
        {
            this.BlogId = int.Parse(userBlog.BlogId);
            this.BlogName = userBlog.BlogName;
            this.Url = userBlog.Url;
            using (MD5 md5 = MD5.Create())
            {
                byte[] hash = md5.ComputeHash(Encoding.Default.GetBytes(this.Url));
                Guid result = new Guid(hash);
                Guid = result.ToString();
            }
            this.XmlRpc = userBlog.XmlRpc;
            this.IsAdmin = userBlog.IsAdmin;
        }

        public bool IsValid()
        {
            if (string.IsNullOrEmpty(BlogName)) return false;
            if (string.IsNullOrEmpty(Url)) return false;
            if (string.IsNullOrEmpty(XmlRpc)) return false;

            return true;
        }
    }
}
