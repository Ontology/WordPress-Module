﻿using OntologyAppDBConnector.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace WordPress_Module
{
    [OntologyClass(IdClass = "3bb4b05d8fcb48b7a1ac0fb42a265c38")]
    public class TermItem
    {
        
        public string Id { get; set; }
        [OntologyObject(PropertyType = PropertyType.Guid)]
        public string Guid { get; set; }
        [OntologyObject(PropertyType = PropertyType.Name)]
        public string Name { get; set; }
        public string Slug { get; set; }
        public string Group { get; set; }
        public string TaxonomyId { get; set; }
        public string Taxonomy { get; set; }
        public string Description { get; set; }
        public string Parent { get; set; }
        public int Count { get; set; }

        public TermItem()
        {

        }
        public TermItem(WordPressSharp.Models.Term termItem)
        {
            this.Id = termItem.Id;
            

            this.Name = termItem.Name;
            using (MD5 md5 = MD5.Create())
            {
                byte[] hash = md5.ComputeHash(Encoding.Default.GetBytes(this.Name));
                Guid result = new Guid(hash);
                Guid = result.ToString();
            }
            this.Slug = termItem.Slug;
            this.Group = termItem.TermGroup;
            this.TaxonomyId = termItem.TermTaxonomyId;
            this.Taxonomy = termItem.Taxonomy;
            this.Description = termItem.Description;
            this.Parent = termItem.Parent;
            this.Count = termItem.Count;
        }
    }
}
